CONTENTS OF THIS FILE
---------------------

 * About body class by path
 * Requirements
 * Installation
 * Configuration

ABOUT ESAY META
---------------------

Esay meta module is used to add metatags in esay way.


REQUIREMENTS
------------

This module doesn't require the help of any other modules.

INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module.

CONFIGURATION
-------------
Nothing is configuration.
After enable to this module go to the "Esay Meta" link under development in configuration.
Add title, keywords and description.
This is one time addition after adding one data we can update only on same link.
